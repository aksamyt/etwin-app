import { defineConfig } from "vite";
import { mithrilSvgPlugin } from "./vite-plugin-mithril-svg";

export default defineConfig({
    root: "src/chrome/",
    plugins: [mithrilSvgPlugin()],
    define: {
        "import.meta.env.PACKAGE_VERSION": JSON.stringify(
            process.env.npm_package_version,
        ),
    },
    build: {
        target: "chrome87",
    },
});
