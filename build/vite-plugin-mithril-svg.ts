import { PluginOption } from "vite";
import { readFileSync } from "fs";

/**
 * Applies the following transformations to an SVG file:
 * - add 100% width and height styling to the `<svg>` node;
 * - transform the SVG data to a Mithril VNode using `m.trust()`.
 *
 * For example, this SVG file:
 * ```xml
 * <svg viewBox='0 0 125 80' xmlns='http://www.w3.org/2000/svg'>
 *   <text y="75" font-size="100" font-family="serif">hello</text>
 * </svg>
 * ```
 * will be transformed to this module (indentation added for clarity):
 * ```js
 * import m from "mithril";
 *
 * export default m.trust(`
 *   <svg style="width: 100%; height: 100%" viewBox='0 0 125 80' xmlns='http://www.w3.org/2000/svg'>
 *     <text y="75" font-size="100" font-family="serif">hello</text>
 *   </svg>
 * `);
 * ```
 */
export const mithrilSvgPlugin = (): PluginOption => ({
    name: "mithril-svg",
    transform(_, id) {
        if (/\.svg$/.test(id)) {
            const code = readFileSync(id)
                .toString()
                .replace("<svg", `<svg style="width: 100%; height: 100%"`)
                .replace(/[`$\\]/g, "\\$&");
            return `import m from "mithril"; export default m.trust(\`${code}\`);`;
        }
        return null;
    },
});
