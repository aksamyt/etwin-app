import Path from "path";
import Fs from "fs";
import esbuild from "esbuild";

function symlink(from: string, to: string) {
    const target = Path.relative(Path.dirname(from), to);
    Fs.symlink(target, from, "dir", err => {
        if (err) {
            if (err.code !== "EEXIST") {
                console.error("error:", err.message);
                process.exit(1);
            }
        } else {
            console.log("ln", JSON.stringify(from), "->", JSON.stringify(to));
        }
    });
}

try {
    const result = esbuild.buildSync({
        bundle: true,
        entryPoints: ["src/electron/index.ts", "src/electron/preload.ts"],
        outdir: "dev",
        external: ["electron"],
        target: "chrome87",
        define: {
            "import.meta.env": JSON.stringify({
                DEV: true,
                PACKAGE_VERSION: process.env.npm_package_version,
            }),
        },
        platform: "node",
        metafile: true,
    });

    if (result.warnings.length) {
        esbuild
            .formatMessagesSync(result.warnings, { kind: "warning" })
            .forEach(console.log);
    }

    if (result.metafile) {
        console.log(esbuild.analyzeMetafileSync(result.metafile));
    }

    symlink("dev/plugins", "src/electron/plugins");
} catch {
    process.exit(1);
}
