import Electron from "electron";
import { Router } from "./Router";

const modKey = process.platform === "darwin" ? "meta" : "control";
const CONTROL = 1;
const SHIFT = 2;
const ALT = 4;

export class BrowserViewManager {
    #tabs = new Map<string, Electron.BrowserView>();
    #shown: Electron.BrowserView | null = null;

    constructor(public win: Electron.BrowserWindow, public router: Router) {}

    new(id: string) {
        const view = new Electron.BrowserView({
            webPreferences: {
                defaultEncoding: "utf8",
                contextIsolation: true, // just to be sure
                nodeIntegration: false, // just to be sure
                plugins: true,
                sandbox: true,
                backgroundThrottling: true,
            },
        });
        view.webContents.on("new-window", (ev, url, _, disposition) => {
            this.router.postMessage([
                "keybind:newtab",
                //TODO: can we do this? Is it safe?
                { background: disposition === "background-tab", url },
            ]);
            ev.preventDefault();
        });
        view.webContents.on("did-navigate", () => {
            this.router.postMessage([
                "tab:history",
                {
                    id,
                    canGoBack: view.webContents.canGoBack(),
                    canGoForward: view.webContents.canGoForward(),
                },
            ]);
        });
        view.webContents.on("did-stop-loading", () => {
            this.router.postMessage(["tab:stoploading", { id }]);
            view == this.#shown && this.#shouldReallyShow(id, view);
        });
        view.webContents.on("did-start-navigation", (_, url, __, isMain) => {
            isMain && this.router.postMessage(["tab:url", { id, url }]);
        });
        view.webContents.on("did-start-loading", () =>
            this.router.postMessage(["tab:loading", { id }]),
        );
        view.webContents.on("page-favicon-updated", (_, favicons) => {
            this.router.postMessage(["tab:favicons", { id, favicons }]);
        });
        view.webContents.on("page-title-updated", (_, title) => {
            this.router.postMessage(["tab:title", { id, title }]);
        });
        view.webContents.on("before-input-event", this.inputListener);
        this.#tabs.set(id, view);
    }

    #shouldReallyShow(id: string, view: Electron.BrowserView) {
        this.#shown = view;
        if (view.webContents.getURL().startsWith("about:")) {
            this.win.setBrowserView(null);
            this.router.postMessage(["tab:favicons", { id, favicons: [] }]);
        } else {
            this.win.setBrowserView(view);
            const { width, height } = this.win.getBounds();
            //TODO: grab this `75` from the CSS or something
            view.setBounds({ x: 0, y: 75, width, height: height - 75 });
            view.setAutoResize({ width: true, height: true });
        }
    }

    #iterShownWebContents(f: (wc: Electron.WebContents) => void) {
        this.win.getBrowserView() != null && f(this.#shown!.webContents);
    }

    #iterTabWebContents(id: string, f: (wc: Electron.WebContents) => void) {
        const view = this.#tabs.get(id);
        view && f(view.webContents);
    }

    inputListener = (ev: Electron.Event, input: Electron.Input) => {
        if (input.type !== "keyDown") {
            return;
        }
        const mods =
            (+input[modKey] << 0) | (+input.shift << 1) | (+input.alt << 2);
        if (mods === 0) {
            switch (input.key) {
                case "F5":
                    this.#iterShownWebContents(wc => wc.reload());
                    break;
                case "F12":
                    this.#iterShownWebContents(wc => wc.openDevTools());
                    break;
                default:
                    return;
            }
        } else if (mods === CONTROL) {
            console.log(input.key);
            switch (input.key) {
                case "l":
                    this.router.postMessage(["keybind:focusinput", null]);
                    break;
                case "r":
                    this.#shown?.webContents.reload();
                    break;
                case "t":
                    this.router.postMessage(["keybind:newtab", null]);
                    break;
                case "w":
                    this.router.postMessage(["keybind:closeactivetab", null]);
                    break;
                default:
                    return;
            }
        } else if (mods === ALT) {
            console.log(input.key);
            switch (input.key) {
                case "ArrowLeft":
                    this.#shown?.webContents.goBack();
                    break;
                case "ArrowRight":
                    this.#shown?.webContents.goForward();
                    break;
                default:
                    return;
            }
        } else if (mods & CONTROL) {
            switch (input.key) {
                case "Tab":
                    this.router.postMessage([
                        mods & SHIFT ? "keybind:tableft" : "keybind:tabright",
                        null,
                    ]);
                    break;
                case "+":
                    this.#iterShownWebContents(
                        wc => wc.zoomLevel < 9 && ++wc.zoomLevel,
                    );
                    break;
                case "-":
                    this.#iterShownWebContents(
                        wc => wc.zoomLevel > -9 && --wc.zoomLevel,
                    );
                    break;
                case "0":
                    this.#iterShownWebContents(wc => wc.setZoomLevel(0));
                    break;
                default:
                    return;
            }
        } else {
            return;
        }
        ev.preventDefault();
    };

    show(id: string) {
        const view = this.#tabs.get(id);
        view && this.#shouldReallyShow(id, view);
    }

    delete(id: string) {
        this.#iterTabWebContents(id, wc => {
            wc.destroy();
            this.#tabs.delete(id);
        });
    }

    goto(id: string, url: string) {
        this.#iterTabWebContents(id, wc => wc.loadURL(url));
    }

    reload(id: string) {
        this.#iterTabWebContents(id, wc => wc.reload());
    }

    back(id: string) {
        this.#iterTabWebContents(id, wc => wc.goBack());
    }

    forward(id: string) {
        this.#iterTabWebContents(id, wc => wc.goForward());
    }
}
