import Fs from "fs";
import Path from "path";

const platformFileName: Partial<Record<NodeJS.Platform, string>> = {
    win32: "pepflashplayer.dll",
    darwin: "PepperFlashPlayer.plugin",
    linux: "libpepflashplayer.so",
};

export function findFlashPlugin(): { path: string; version: string } | null {
    const fileName = platformFileName[process.platform] ?? "flashplayer";
    const version = "32.0.0.465";
    const base = Path.join(__dirname, "plugins", "flash");
    let path: string;

    // try with platform + arch
    path = Path.join(base, `${process.platform}-${process.arch}`, fileName);
    if (Fs.existsSync(path)) {
        return { path, version };
    }

    // try with platform
    path = Path.join(base, process.platform, fileName);
    if (Fs.existsSync(path)) {
        return { path, version };
    }

    return null;
}

type MmsCfg = {
    [K: string]: string | number | Array<string | number>;
};

const mmsBase: MmsCfg = {
    EOLUninstallDisable: 1,
    SilentAutoUpdateEnable: 0,
    EnableAllowList: 1,
    AutoUpdateDisable: 1,
    ErrorReportingEnable: 1,
    AllowListUrlPattern: [
        "blob:*",
        "file:*",
        "*://eternalfest.net",
        "*://*.eternalfest.net",
        "*://eternal-twin.net",
        "*://*.eternal-twin.net",
        "*://eternaltwin.net",
        "*://*.eternaltwin.net",
        "*://fevermap.org/",
        "*://*.fevermap.org/",
        "*://twinoid.com",
        "*://*.twinoid.com",
        "*://twinoid.es",
        "*://*.twinoid.es",
        "*://hammerfest.es",
        "*://*.hammerfest.es",
        "*://hammerfest.fr",
        "*://*.hammerfest.fr",
        "*://hfest.net",
        "*://*.hfest.net",
        "*://dinorpg.de",
        "*://*.dinorpg.de",
        "*://dinorpg.com",
        "*://*.dinorpg.com",
        "*://hordes.fr",
        "*://*.hordes.fr",
        "*://die2nite.com",
        "*://*.die2nite.com",
        "*://zombinoia.com",
        "*://*.zombinoia.com",
        "*://dieverdammten.de",
        "*://*.dieverdammten.de",
        "*://muxxu.com",
        "*://*.muxxu.com",
        "*://mush.vg",
        "*://*.mush.vg",
        "*://alphabounce.com",
        "*://*.alphabounce.com",
        "*://kadokado.com",
        "*://*.kadokado.com",
        "*://teacher-story.com",
        "*://*.teacher-story.com",
        "*://naturalchimie.com",
        "*://*.naturalchimie.com",
        "*://arkadeo.com",
        "*://*.arkadeo.com",
        "*://street-writer.com",
        "*://*.street-writer.com",
        "*://rockfaller.com",
        "*://*.rockfaller.com",
        "*://monster-hotel.net",
        "*://*.monster-hotel.net",
        "*://minitroopers.com",
        "*://*.minitroopers.com",
        "*://minitroopers.es",
        "*://*.minitroopers.es",
        "*://minitroopers.fr",
        "*://*.minitroopers.fr",
        "*://popotamo.com",
        "*://*.popotamo.com",
        "*://cafejeux.com",
        "*://*.cafejeux.com",
        "*://carapass.com",
        "*://*.carapass.com",
        "*://croquemonster.com",
        "*://*.croquemonster.com",
        "*://dinocard.net",
        "*://*.dinocard.net",
        "*://dinoparc.com",
        "*://*.dinoparc.com",
        "*://frutiparc.com",
        "*://*.frutiparc.com",
        "*://hyperliner.com",
        "*://*.hyperliner.com",
        "*://miniville.fr",
        "*://*.miniville.fr",
        "*://myminicity.com",
        "*://*.myminicity.com",
        "*://skywar.net",
        "*://*.skywar.net",
        "*://dailymotion.com",
        "*://*.dailymotion.com",
        "*://localhost",
        "*://*.localhost",
        "*://localhost:3000",
        "*://localhost:50317",
        "*://*.localhost:50317",
        "*://127.0.0.1",
        "*://*.127.0.0.1",
        "*://127.0.0.1:50317",
        "*://*.127.0.0.1:50317",
        "*://dinorpg.eternaltwin.org",
        "*://*.dinorpg.eternaltwin.org",
        "*://dinorpg.localhost",
        "*://*.dinorpg.localhost",
    ],
};

function formatMms(mms: MmsCfg): string {
    let blob = "";
    Object.keys(mms)
        .sort()
        .forEach(key => {
            const value = mms[key];
            if (value instanceof Array) {
                value.forEach(v => (blob += `${key}=${v}\n`));
            } else {
                blob += `${key}=${value}\n`;
            }
        });
    return blob;
}

export function generateMmsContents() {
    return formatMms(mmsBase);
}
