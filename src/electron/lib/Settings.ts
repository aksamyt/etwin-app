import Fs from "fs";
import Path from "path";

import Electron from "electron";

import { generateMmsContents } from "./flash";

export class Settings {
    #userData = Electron.app.getPath("userData");
    #settingsPath = Path.join(this.#userData, "settings.json");
    bounds: Partial<Electron.Rectangle> = {};

    constructor() {
        this.#touchFlashConfig();
        try {
            const buf = Fs.readFileSync(this.#settingsPath);
            const data = JSON.parse(buf.toString());
            this.bounds = data["bounds"];
        } catch {}
    }

    save() {
        const data = JSON.stringify({
            bounds: this.bounds,
        });
        Fs.writeFileSync(this.#settingsPath, data);
    }

    #touchFlashConfig() {
        const systemDir = Path.join(
            this.#userData,
            "Pepper Data",
            "Shockwave Flash",
            "System",
        );
        const mmsCfgPath = Path.join(systemDir, "mms.cfg");
        if (!Fs.existsSync(mmsCfgPath)) {
            Fs.mkdirSync(systemDir, { recursive: true });
            const mmsCfg = generateMmsContents();
            Fs.writeFileSync(mmsCfgPath, mmsCfg);
            console.log("wrote cfg to", mmsCfgPath);
        }
    }
}
