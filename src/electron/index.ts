import Path from "path";

import Electron from "electron";
import semverGt from "semver/functions/gt";

import type { RendererMessage } from "../common/messages";
import { BrowserViewManager } from "./lib/BrowserViewManager";
import { findFlashPlugin } from "./lib/flash";
import type { Router } from "./lib/Router";
import { Settings } from "./lib/Settings";
import { getAppReleases, getGames } from "../common/etwinApi";
import { nodeFetch } from "../common/fetch";

const settings = new Settings();

const flash = findFlashPlugin();
if (flash) {
    Electron.app.commandLine.appendSwitch("no-sandbox");
    Electron.app.commandLine.appendSwitch("ppapi-flash-path", flash.path);
    Electron.app.commandLine.appendSwitch("ppapi-flash-version", flash.version);
}

Electron.Menu.setApplicationMenu(null);

/**
 * The Etwin app is an Electron app, thus it is divided in multiple processes.
 * For convenience, I chose to group the app pages (like the homepage) under the
 * `etwin:` protocol. They’re still technically part of the chrome, but they’re
 * shown at the same place as the “true” web pages, so they must feel the same
 * to Users.
 *
 * When a `BrowserView` loads an `etwin:` page, it should remove itself from the
 * window and let the User see the real page shown by the chrome. We do this to
 * avoid desyncs between the Main and the Renderer and to keep the browsing
 * state on the Main process.
 *
 * Alas, this doesn’t work: `BrowserView`s refuse to load unknown protocols,
 * and we can’t support custom protocols like we can with the main window.
 *
 * The solution is to use the `about:` protocol. It is supported by the
 * `BrowserView` and doesn’t render anything. But we can’t just replace the
 * protocol (`about:home` is transformed into `about:blank#blocked`)! So we use
 * the fragment to store the true page, add a translation layer, and both
 * processes are happy.
 *
 * That’s why we wrap the router. It’s not elegant but, hey, it works!
 */
function wrapRouter(router: Router): Router {
    const replaceUrl = (o: { url: string }, rx: RegExp, s: string) =>
        (o.url = o.url.replace(rx, s));
    const { on, postMessage } = router;
    router.on = (event, listener) =>
        event === "message"
            ? on.call(router, event, ev => {
                  const pl = ev.data[1];
                  "url" in pl && replaceUrl(pl, /^etwin:/, "about:blank#");
                  return listener(ev);
              })
            : on.call(router, event, listener);
    //NOTE: don’t forget to wrap `once` if we ever use it!
    router.postMessage = message => {
        const pl = message[1];
        pl && "url" in pl && replaceUrl(pl, /^about:blank#/, "etwin:");
        postMessage.call(router, message);
    };
    return router;
}

async function queryGames(router: Router) {
    const games = await getGames(nodeFetch);
    router.postMessage(["data:games", { games }]);
    setTimeout(() => queryGames(router), 1000 * 60 * 30);
}

async function queryLastVersion(router: Router) {
    const { latest } = await getAppReleases(nodeFetch);
    console.log({ lhs: latest.version, rhs: import.meta.env.PACKAGE_VERSION });
    if (semverGt(latest.version, import.meta.env.PACKAGE_VERSION)) {
        router.postMessage(["data:update", { newVersion: latest.version }]);
    }
    setTimeout(() => queryLastVersion(router), 1000 * 60 * 30);
}

function createWindow() {
    const win = new Electron.BrowserWindow({
        width: settings.bounds.width,
        height: settings.bounds.height,
        x: settings.bounds.x,
        y: settings.bounds.y,
        webPreferences: {
            defaultEncoding: "utf8",
            contextIsolation: true,
            nodeIntegration: false, // just to be sure
            preload: Path.join(Electron.app.getAppPath(), "preload.js"),
            sandbox: true,
            plugins: true,
        },
    });

    if (import.meta.env.DEV) {
        win.loadURL(`http://localhost:${process.env["PORT"] ?? 3000}`);
        win.webContents.openDevTools({ mode: "detach" });
    }

    win.on("close", () => {
        settings.bounds = win.getBounds();
    });

    Electron.ipcMain.once("router", ({ ports }) => {
        const router: Router = wrapRouter(ports[0]);
        router.start();
        queryGames(router);
        queryLastVersion(router);
        const tabManager = new BrowserViewManager(win, router);
        router.on("message", ev => {
            const [type, pl] = ev.data as RendererMessage;
            import.meta.env.DEV && console.dir({ type, pl });
            switch (type) {
                case "tab:new":
                    tabManager.new(pl.id);
                    tabManager.goto(pl.id, pl.url);
                    break;
                case "tab:show":
                    tabManager.show(pl.id);
                    break;
                case "tab:goto":
                    tabManager.goto(pl.id, pl.url);
                    break;
                case "tab:close":
                    tabManager.delete(pl.id);
                    break;
                case "tab:reload":
                    tabManager.reload(pl.id);
                    break;
                case "tab:back":
                    tabManager.back(pl.id);
                    break;
                case "tab:forward":
                    tabManager.forward(pl.id);
                    break;
            }
        });
        win.webContents.on("before-input-event", tabManager.inputListener);
    });
}

Electron.app.on("will-quit", () => {
    settings.save();
});

Electron.app.on("ready", () => {
    createWindow();
});

// fix <https://github.com/advisories/GHSA-3p22-ghq8-v749>
Electron.app.on("web-contents-created", (_, webContents) => {
    webContents.on("select-bluetooth-device", (event, _, callback) => {
        // Prevent default behavior
        event.preventDefault();
        // Cancel the request
        callback("");
    });
});
