import BannerPng from "../assets/banner.png";
import m from "mithril";

const $Img = `img[alt=eternaltwin logo][draggable=''][height=109]
    .block
    .w-100%
    .b.bw-b-1/10.etwin-bc-soft-purple-white-soft-purple
    .etwin-bg-dark-red
    .object-fit-none
`;

export const EtwinHeader = m("header", m($Img, { src: BannerPng }));
