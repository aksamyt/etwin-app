import m from "mithril";
import { EtwinUrl } from "../../common/constants";

import House from "../assets/icon-house.svg";
import LeftArrow from "../assets/icon-left-arrow.svg";
import Reload from "../assets/icon-reload.svg";
import RightArrow from "../assets/icon-right-arrow.svg";
import { autobind } from "../lib/autobind";
import { BrowserTab } from "./browser-tab";
import { UiButton } from "./ui-button";
import { UiInput } from "./ui-input";

export namespace BrowserBar {
    export type State = {
        inputRef: HTMLInputElement | null;
    };
    export type Actions = {
        setInputRef: (ref: HTMLInputElement) => void;
    };
}

const $Bar = `.
    .flex.items-center.gap-1/6
    .p-1/6
    .bg0
`;

export const BrowserBar = (): m.Component<{
    state: BrowserBar.State;
    actions: BrowserBar.Actions;
    activeTab: BrowserTab.State;
    handleBack: () => void;
    handleForward: () => void;
    handleReload: () => void;
    handleUrl: (url: URL) => void;
    handleHome: () => void;
}> => ({
    view: ({ attrs }) =>
        m($Bar, [
            m(UiButton, {
                icon: LeftArrow,
                size: "big",
                disabled: !attrs.activeTab.back,
                onclick: attrs.handleBack,
            }),
            m(UiButton, {
                icon: RightArrow,
                size: "big",
                disabled: !attrs.activeTab.forward,
                onclick: attrs.handleForward,
            }),
            m(UiButton, {
                icon: Reload,
                size: "big",
                onclick: attrs.handleReload,
            }),
            m(UiInput, {
                spellcheck: false,
                onkeydown: (ev: KeyboardEvent & m.Event) => {
                    ev.redraw = false;
                    const input = ev.target as HTMLInputElement;
                    if (ev.code.endsWith("Enter") && !ev.repeat) {
                        input.blur();
                        attrs.handleUrl(safeUrl(input.value));
                    } else if (ev.code === "Escape") {
                        input.blur();
                    }
                },
                value: attrs.activeTab.url.href,
                oncreate: ({ dom }) =>
                    attrs.actions.setInputRef(dom as HTMLInputElement),
                // we need to catch the click before the `onblur` event
                onmousedown: (ev: Event) => {
                    const input = ev.target as HTMLInputElement;
                    if (document.activeElement !== input) {
                        input.select();
                        ev.preventDefault();
                    }
                },
            }),
            m(UiButton, {
                icon: House,
                onclick: attrs.handleHome,
                size: "big",
            }),
        ]),
});

function safeUrl(raw: string): URL {
    try {
        return new URL(raw);
    } catch {
        return EtwinUrl.Home;
    }
}

BrowserBar.makeState = (): BrowserBar.State => ({
    inputRef: null,
});

BrowserBar.makeActions = (state: BrowserBar.State): BrowserBar.Actions =>
    autobind({
        setInputRef: ref => (state.inputRef = ref),
    });
