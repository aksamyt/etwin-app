import m from "mithril";
import { EtwinLink } from "./etwin-link";

const $Div = `.
    .inline-block
    .float-right
    .br-1/5
    .etwin-btn-styles
`;

export const EtwinNewVersionButton = (): m.Component<{
    to: string;
    text: string;
    icon: string;
}> => ({
    view: ({ attrs }) =>
        m(EtwinLink, { to: attrs.to }, [
            m($Div, [
                m("img.float-left.mr-1/10.crisp", { src: attrs.icon }),
                attrs.text,
            ]),
        ]),
});
