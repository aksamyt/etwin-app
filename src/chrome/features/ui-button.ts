import cx, { Argument } from "classnames";
import m from "mithril";

type ButtonSize = "big" | "medium" | "small";

const sizeClass: Record<ButtonSize, string> = {
    big: `
        w-1 h-1
        p-1/6
        br-1/5
    `,
    medium: `
        w-4/5 h-4/5
        p-1/6
        br-1/5
    `,
    small: `
        w-2/3 h-2/3
        p-1/10
        br-100%
    `,
};

const $Button = `button.
    .border-box
    .active:bg-fg2
    .trans-bg-1/2
`;

export const UiButton = (): m.Component<{
    icon: m.Vnode;
    onclick: EventListener;
    disabled?: boolean;
    size?: ButtonSize;
    class?: Argument | Argument[];
}> => ({
    view: ({ attrs }) =>
        m(
            $Button,
            {
                class: cx(sizeClass[attrs.size ?? "medium"], attrs.class, {
                    fg1: attrs.disabled,
                    "hover:bg-fg2-50": !attrs.disabled,
                }),
                disabled: attrs.disabled,
                onclick: attrs.onclick,
            },
            attrs.icon,
        ),
});
