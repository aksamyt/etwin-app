var __create = Object.create;
var __defProp = Object.defineProperty;
var __getOwnPropDesc = Object.getOwnPropertyDescriptor;
var __getOwnPropNames = Object.getOwnPropertyNames;
var __getOwnPropSymbols = Object.getOwnPropertySymbols;
var __getProtoOf = Object.getPrototypeOf;
var __hasOwnProp = Object.prototype.hasOwnProperty;
var __propIsEnum = Object.prototype.propertyIsEnumerable;
var __defNormalProp = (obj, key, value) => key in obj ? __defProp(obj, key, { enumerable: true, configurable: true, writable: true, value }) : obj[key] = value;
var __spreadValues = (a, b) => {
  for (var prop in b || (b = {}))
    if (__hasOwnProp.call(b, prop))
      __defNormalProp(a, prop, b[prop]);
  if (__getOwnPropSymbols)
    for (var prop of __getOwnPropSymbols(b)) {
      if (__propIsEnum.call(b, prop))
        __defNormalProp(a, prop, b[prop]);
    }
  return a;
};
var __markAsModule = (target) => __defProp(target, "__esModule", { value: true });
var __esm = (fn, res) => function __init() {
  return fn && (res = (0, fn[Object.keys(fn)[0]])(fn = 0)), res;
};
var __commonJS = (cb, mod) => function __require() {
  return mod || (0, cb[Object.keys(cb)[0]])((mod = { exports: {} }).exports, mod), mod.exports;
};
var __reExport = (target, module2, desc) => {
  if (module2 && typeof module2 === "object" || typeof module2 === "function") {
    for (let key of __getOwnPropNames(module2))
      if (!__hasOwnProp.call(target, key) && key !== "default")
        __defProp(target, key, { get: () => module2[key], enumerable: !(desc = __getOwnPropDesc(module2, key)) || desc.enumerable });
  }
  return target;
};
var __toModule = (module2) => {
  return __reExport(__markAsModule(__defProp(module2 != null ? __create(__getProtoOf(module2)) : {}, "default", module2 && module2.__esModule && "default" in module2 ? { get: () => module2.default, enumerable: true } : { value: module2, enumerable: true })), module2);
};

// <define:import.meta.env>
var DEV, PACKAGE_VERSION, define_import_meta_env_default;
var init_define_import_meta_env = __esm({
  "<define:import.meta.env>"() {
    DEV = true;
    PACKAGE_VERSION = "0.0.0+uwuwuwu";
    define_import_meta_env_default = { DEV, PACKAGE_VERSION };
  }
});

// node_modules/semver/internal/debug.js
var require_debug = __commonJS({
  "node_modules/semver/internal/debug.js"(exports2, module2) {
    init_define_import_meta_env();
    var debug = typeof process === "object" && process.env && process.env.NODE_DEBUG && /\bsemver\b/i.test(process.env.NODE_DEBUG) ? (...args) => console.error("SEMVER", ...args) : () => {
    };
    module2.exports = debug;
  }
});

// node_modules/semver/internal/constants.js
var require_constants = __commonJS({
  "node_modules/semver/internal/constants.js"(exports2, module2) {
    init_define_import_meta_env();
    var SEMVER_SPEC_VERSION = "2.0.0";
    var MAX_LENGTH = 256;
    var MAX_SAFE_INTEGER = Number.MAX_SAFE_INTEGER || 9007199254740991;
    var MAX_SAFE_COMPONENT_LENGTH = 16;
    module2.exports = {
      SEMVER_SPEC_VERSION,
      MAX_LENGTH,
      MAX_SAFE_INTEGER,
      MAX_SAFE_COMPONENT_LENGTH
    };
  }
});

// node_modules/semver/internal/re.js
var require_re = __commonJS({
  "node_modules/semver/internal/re.js"(exports2, module2) {
    init_define_import_meta_env();
    var { MAX_SAFE_COMPONENT_LENGTH } = require_constants();
    var debug = require_debug();
    exports2 = module2.exports = {};
    var re = exports2.re = [];
    var src = exports2.src = [];
    var t = exports2.t = {};
    var R = 0;
    var createToken = (name, value, isGlobal) => {
      const index = R++;
      debug(index, value);
      t[name] = index;
      src[index] = value;
      re[index] = new RegExp(value, isGlobal ? "g" : void 0);
    };
    createToken("NUMERICIDENTIFIER", "0|[1-9]\\d*");
    createToken("NUMERICIDENTIFIERLOOSE", "[0-9]+");
    createToken("NONNUMERICIDENTIFIER", "\\d*[a-zA-Z-][a-zA-Z0-9-]*");
    createToken("MAINVERSION", `(${src[t.NUMERICIDENTIFIER]})\\.(${src[t.NUMERICIDENTIFIER]})\\.(${src[t.NUMERICIDENTIFIER]})`);
    createToken("MAINVERSIONLOOSE", `(${src[t.NUMERICIDENTIFIERLOOSE]})\\.(${src[t.NUMERICIDENTIFIERLOOSE]})\\.(${src[t.NUMERICIDENTIFIERLOOSE]})`);
    createToken("PRERELEASEIDENTIFIER", `(?:${src[t.NUMERICIDENTIFIER]}|${src[t.NONNUMERICIDENTIFIER]})`);
    createToken("PRERELEASEIDENTIFIERLOOSE", `(?:${src[t.NUMERICIDENTIFIERLOOSE]}|${src[t.NONNUMERICIDENTIFIER]})`);
    createToken("PRERELEASE", `(?:-(${src[t.PRERELEASEIDENTIFIER]}(?:\\.${src[t.PRERELEASEIDENTIFIER]})*))`);
    createToken("PRERELEASELOOSE", `(?:-?(${src[t.PRERELEASEIDENTIFIERLOOSE]}(?:\\.${src[t.PRERELEASEIDENTIFIERLOOSE]})*))`);
    createToken("BUILDIDENTIFIER", "[0-9A-Za-z-]+");
    createToken("BUILD", `(?:\\+(${src[t.BUILDIDENTIFIER]}(?:\\.${src[t.BUILDIDENTIFIER]})*))`);
    createToken("FULLPLAIN", `v?${src[t.MAINVERSION]}${src[t.PRERELEASE]}?${src[t.BUILD]}?`);
    createToken("FULL", `^${src[t.FULLPLAIN]}$`);
    createToken("LOOSEPLAIN", `[v=\\s]*${src[t.MAINVERSIONLOOSE]}${src[t.PRERELEASELOOSE]}?${src[t.BUILD]}?`);
    createToken("LOOSE", `^${src[t.LOOSEPLAIN]}$`);
    createToken("GTLT", "((?:<|>)?=?)");
    createToken("XRANGEIDENTIFIERLOOSE", `${src[t.NUMERICIDENTIFIERLOOSE]}|x|X|\\*`);
    createToken("XRANGEIDENTIFIER", `${src[t.NUMERICIDENTIFIER]}|x|X|\\*`);
    createToken("XRANGEPLAIN", `[v=\\s]*(${src[t.XRANGEIDENTIFIER]})(?:\\.(${src[t.XRANGEIDENTIFIER]})(?:\\.(${src[t.XRANGEIDENTIFIER]})(?:${src[t.PRERELEASE]})?${src[t.BUILD]}?)?)?`);
    createToken("XRANGEPLAINLOOSE", `[v=\\s]*(${src[t.XRANGEIDENTIFIERLOOSE]})(?:\\.(${src[t.XRANGEIDENTIFIERLOOSE]})(?:\\.(${src[t.XRANGEIDENTIFIERLOOSE]})(?:${src[t.PRERELEASELOOSE]})?${src[t.BUILD]}?)?)?`);
    createToken("XRANGE", `^${src[t.GTLT]}\\s*${src[t.XRANGEPLAIN]}$`);
    createToken("XRANGELOOSE", `^${src[t.GTLT]}\\s*${src[t.XRANGEPLAINLOOSE]}$`);
    createToken("COERCE", `${"(^|[^\\d])(\\d{1,"}${MAX_SAFE_COMPONENT_LENGTH}})(?:\\.(\\d{1,${MAX_SAFE_COMPONENT_LENGTH}}))?(?:\\.(\\d{1,${MAX_SAFE_COMPONENT_LENGTH}}))?(?:$|[^\\d])`);
    createToken("COERCERTL", src[t.COERCE], true);
    createToken("LONETILDE", "(?:~>?)");
    createToken("TILDETRIM", `(\\s*)${src[t.LONETILDE]}\\s+`, true);
    exports2.tildeTrimReplace = "$1~";
    createToken("TILDE", `^${src[t.LONETILDE]}${src[t.XRANGEPLAIN]}$`);
    createToken("TILDELOOSE", `^${src[t.LONETILDE]}${src[t.XRANGEPLAINLOOSE]}$`);
    createToken("LONECARET", "(?:\\^)");
    createToken("CARETTRIM", `(\\s*)${src[t.LONECARET]}\\s+`, true);
    exports2.caretTrimReplace = "$1^";
    createToken("CARET", `^${src[t.LONECARET]}${src[t.XRANGEPLAIN]}$`);
    createToken("CARETLOOSE", `^${src[t.LONECARET]}${src[t.XRANGEPLAINLOOSE]}$`);
    createToken("COMPARATORLOOSE", `^${src[t.GTLT]}\\s*(${src[t.LOOSEPLAIN]})$|^$`);
    createToken("COMPARATOR", `^${src[t.GTLT]}\\s*(${src[t.FULLPLAIN]})$|^$`);
    createToken("COMPARATORTRIM", `(\\s*)${src[t.GTLT]}\\s*(${src[t.LOOSEPLAIN]}|${src[t.XRANGEPLAIN]})`, true);
    exports2.comparatorTrimReplace = "$1$2$3";
    createToken("HYPHENRANGE", `^\\s*(${src[t.XRANGEPLAIN]})\\s+-\\s+(${src[t.XRANGEPLAIN]})\\s*$`);
    createToken("HYPHENRANGELOOSE", `^\\s*(${src[t.XRANGEPLAINLOOSE]})\\s+-\\s+(${src[t.XRANGEPLAINLOOSE]})\\s*$`);
    createToken("STAR", "(<|>)?=?\\s*\\*");
    createToken("GTE0", "^\\s*>=\\s*0.0.0\\s*$");
    createToken("GTE0PRE", "^\\s*>=\\s*0.0.0-0\\s*$");
  }
});

// node_modules/semver/internal/parse-options.js
var require_parse_options = __commonJS({
  "node_modules/semver/internal/parse-options.js"(exports2, module2) {
    init_define_import_meta_env();
    var opts = ["includePrerelease", "loose", "rtl"];
    var parseOptions = (options) => !options ? {} : typeof options !== "object" ? { loose: true } : opts.filter((k) => options[k]).reduce((options2, k) => {
      options2[k] = true;
      return options2;
    }, {});
    module2.exports = parseOptions;
  }
});

// node_modules/semver/internal/identifiers.js
var require_identifiers = __commonJS({
  "node_modules/semver/internal/identifiers.js"(exports2, module2) {
    init_define_import_meta_env();
    var numeric = /^[0-9]+$/;
    var compareIdentifiers = (a, b) => {
      const anum = numeric.test(a);
      const bnum = numeric.test(b);
      if (anum && bnum) {
        a = +a;
        b = +b;
      }
      return a === b ? 0 : anum && !bnum ? -1 : bnum && !anum ? 1 : a < b ? -1 : 1;
    };
    var rcompareIdentifiers = (a, b) => compareIdentifiers(b, a);
    module2.exports = {
      compareIdentifiers,
      rcompareIdentifiers
    };
  }
});

// node_modules/semver/classes/semver.js
var require_semver = __commonJS({
  "node_modules/semver/classes/semver.js"(exports2, module2) {
    init_define_import_meta_env();
    var debug = require_debug();
    var { MAX_LENGTH, MAX_SAFE_INTEGER } = require_constants();
    var { re, t } = require_re();
    var parseOptions = require_parse_options();
    var { compareIdentifiers } = require_identifiers();
    var SemVer = class {
      constructor(version, options) {
        options = parseOptions(options);
        if (version instanceof SemVer) {
          if (version.loose === !!options.loose && version.includePrerelease === !!options.includePrerelease) {
            return version;
          } else {
            version = version.version;
          }
        } else if (typeof version !== "string") {
          throw new TypeError(`Invalid Version: ${version}`);
        }
        if (version.length > MAX_LENGTH) {
          throw new TypeError(`version is longer than ${MAX_LENGTH} characters`);
        }
        debug("SemVer", version, options);
        this.options = options;
        this.loose = !!options.loose;
        this.includePrerelease = !!options.includePrerelease;
        const m = version.trim().match(options.loose ? re[t.LOOSE] : re[t.FULL]);
        if (!m) {
          throw new TypeError(`Invalid Version: ${version}`);
        }
        this.raw = version;
        this.major = +m[1];
        this.minor = +m[2];
        this.patch = +m[3];
        if (this.major > MAX_SAFE_INTEGER || this.major < 0) {
          throw new TypeError("Invalid major version");
        }
        if (this.minor > MAX_SAFE_INTEGER || this.minor < 0) {
          throw new TypeError("Invalid minor version");
        }
        if (this.patch > MAX_SAFE_INTEGER || this.patch < 0) {
          throw new TypeError("Invalid patch version");
        }
        if (!m[4]) {
          this.prerelease = [];
        } else {
          this.prerelease = m[4].split(".").map((id) => {
            if (/^[0-9]+$/.test(id)) {
              const num = +id;
              if (num >= 0 && num < MAX_SAFE_INTEGER) {
                return num;
              }
            }
            return id;
          });
        }
        this.build = m[5] ? m[5].split(".") : [];
        this.format();
      }
      format() {
        this.version = `${this.major}.${this.minor}.${this.patch}`;
        if (this.prerelease.length) {
          this.version += `-${this.prerelease.join(".")}`;
        }
        return this.version;
      }
      toString() {
        return this.version;
      }
      compare(other) {
        debug("SemVer.compare", this.version, this.options, other);
        if (!(other instanceof SemVer)) {
          if (typeof other === "string" && other === this.version) {
            return 0;
          }
          other = new SemVer(other, this.options);
        }
        if (other.version === this.version) {
          return 0;
        }
        return this.compareMain(other) || this.comparePre(other);
      }
      compareMain(other) {
        if (!(other instanceof SemVer)) {
          other = new SemVer(other, this.options);
        }
        return compareIdentifiers(this.major, other.major) || compareIdentifiers(this.minor, other.minor) || compareIdentifiers(this.patch, other.patch);
      }
      comparePre(other) {
        if (!(other instanceof SemVer)) {
          other = new SemVer(other, this.options);
        }
        if (this.prerelease.length && !other.prerelease.length) {
          return -1;
        } else if (!this.prerelease.length && other.prerelease.length) {
          return 1;
        } else if (!this.prerelease.length && !other.prerelease.length) {
          return 0;
        }
        let i = 0;
        do {
          const a = this.prerelease[i];
          const b = other.prerelease[i];
          debug("prerelease compare", i, a, b);
          if (a === void 0 && b === void 0) {
            return 0;
          } else if (b === void 0) {
            return 1;
          } else if (a === void 0) {
            return -1;
          } else if (a === b) {
            continue;
          } else {
            return compareIdentifiers(a, b);
          }
        } while (++i);
      }
      compareBuild(other) {
        if (!(other instanceof SemVer)) {
          other = new SemVer(other, this.options);
        }
        let i = 0;
        do {
          const a = this.build[i];
          const b = other.build[i];
          debug("prerelease compare", i, a, b);
          if (a === void 0 && b === void 0) {
            return 0;
          } else if (b === void 0) {
            return 1;
          } else if (a === void 0) {
            return -1;
          } else if (a === b) {
            continue;
          } else {
            return compareIdentifiers(a, b);
          }
        } while (++i);
      }
      inc(release, identifier) {
        switch (release) {
          case "premajor":
            this.prerelease.length = 0;
            this.patch = 0;
            this.minor = 0;
            this.major++;
            this.inc("pre", identifier);
            break;
          case "preminor":
            this.prerelease.length = 0;
            this.patch = 0;
            this.minor++;
            this.inc("pre", identifier);
            break;
          case "prepatch":
            this.prerelease.length = 0;
            this.inc("patch", identifier);
            this.inc("pre", identifier);
            break;
          case "prerelease":
            if (this.prerelease.length === 0) {
              this.inc("patch", identifier);
            }
            this.inc("pre", identifier);
            break;
          case "major":
            if (this.minor !== 0 || this.patch !== 0 || this.prerelease.length === 0) {
              this.major++;
            }
            this.minor = 0;
            this.patch = 0;
            this.prerelease = [];
            break;
          case "minor":
            if (this.patch !== 0 || this.prerelease.length === 0) {
              this.minor++;
            }
            this.patch = 0;
            this.prerelease = [];
            break;
          case "patch":
            if (this.prerelease.length === 0) {
              this.patch++;
            }
            this.prerelease = [];
            break;
          case "pre":
            if (this.prerelease.length === 0) {
              this.prerelease = [0];
            } else {
              let i = this.prerelease.length;
              while (--i >= 0) {
                if (typeof this.prerelease[i] === "number") {
                  this.prerelease[i]++;
                  i = -2;
                }
              }
              if (i === -1) {
                this.prerelease.push(0);
              }
            }
            if (identifier) {
              if (this.prerelease[0] === identifier) {
                if (isNaN(this.prerelease[1])) {
                  this.prerelease = [identifier, 0];
                }
              } else {
                this.prerelease = [identifier, 0];
              }
            }
            break;
          default:
            throw new Error(`invalid increment argument: ${release}`);
        }
        this.format();
        this.raw = this.version;
        return this;
      }
    };
    module2.exports = SemVer;
  }
});

// node_modules/semver/functions/compare.js
var require_compare = __commonJS({
  "node_modules/semver/functions/compare.js"(exports2, module2) {
    init_define_import_meta_env();
    var SemVer = require_semver();
    var compare = (a, b, loose) => new SemVer(a, loose).compare(new SemVer(b, loose));
    module2.exports = compare;
  }
});

// node_modules/semver/functions/gt.js
var require_gt = __commonJS({
  "node_modules/semver/functions/gt.js"(exports2, module2) {
    init_define_import_meta_env();
    var compare = require_compare();
    var gt = (a, b, loose) => compare(a, b, loose) > 0;
    module2.exports = gt;
  }
});

// src/electron/index.ts
init_define_import_meta_env();
var import_path3 = __toModule(require("path"));
var import_electron3 = __toModule(require("electron"));
var import_gt = __toModule(require_gt());

// src/electron/lib/BrowserViewManager.ts
init_define_import_meta_env();
var import_electron = __toModule(require("electron"));
var modKey = process.platform === "darwin" ? "meta" : "control";
var CONTROL = 1;
var SHIFT = 2;
var ALT = 4;
var BrowserViewManager = class {
  constructor(win, router) {
    this.win = win;
    this.router = router;
  }
  #tabs = new Map();
  #shown = null;
  new(id) {
    const view = new import_electron.default.BrowserView({
      webPreferences: {
        defaultEncoding: "utf8",
        contextIsolation: true,
        nodeIntegration: false,
        plugins: true,
        sandbox: true,
        backgroundThrottling: true
      }
    });
    view.webContents.on("new-window", (ev, url, _, disposition) => {
      this.router.postMessage([
        "keybind:newtab",
        { background: disposition === "background-tab", url }
      ]);
      ev.preventDefault();
    });
    view.webContents.on("did-navigate", () => {
      this.router.postMessage([
        "tab:history",
        {
          id,
          canGoBack: view.webContents.canGoBack(),
          canGoForward: view.webContents.canGoForward()
        }
      ]);
    });
    view.webContents.on("did-stop-loading", () => {
      this.router.postMessage(["tab:stoploading", { id }]);
      view == this.#shown && this.#shouldReallyShow(id, view);
    });
    view.webContents.on("did-start-navigation", (_, url, __, isMain) => {
      isMain && this.router.postMessage(["tab:url", { id, url }]);
    });
    view.webContents.on("did-start-loading", () => this.router.postMessage(["tab:loading", { id }]));
    view.webContents.on("page-favicon-updated", (_, favicons) => {
      this.router.postMessage(["tab:favicons", { id, favicons }]);
    });
    view.webContents.on("page-title-updated", (_, title) => {
      this.router.postMessage(["tab:title", { id, title }]);
    });
    view.webContents.on("before-input-event", this.inputListener);
    this.#tabs.set(id, view);
  }
  #shouldReallyShow(id, view) {
    this.#shown = view;
    if (view.webContents.getURL().startsWith("about:")) {
      this.win.setBrowserView(null);
      this.router.postMessage(["tab:favicons", { id, favicons: [] }]);
    } else {
      this.win.setBrowserView(view);
      const { width, height } = this.win.getBounds();
      view.setBounds({ x: 0, y: 75, width, height: height - 75 });
      view.setAutoResize({ width: true, height: true });
    }
  }
  #iterShownWebContents(f) {
    this.win.getBrowserView() != null && f(this.#shown.webContents);
  }
  #iterTabWebContents(id, f) {
    const view = this.#tabs.get(id);
    view && f(view.webContents);
  }
  inputListener = (ev, input) => {
    var _a, _b, _c;
    if (input.type !== "keyDown") {
      return;
    }
    const mods = +input[modKey] << 0 | +input.shift << 1 | +input.alt << 2;
    if (mods === 0) {
      switch (input.key) {
        case "F5":
          this.#iterShownWebContents((wc) => wc.reload());
          break;
        case "F12":
          this.#iterShownWebContents((wc) => wc.openDevTools());
          break;
        default:
          return;
      }
    } else if (mods === CONTROL) {
      console.log(input.key);
      switch (input.key) {
        case "l":
          this.router.postMessage(["keybind:focusinput", null]);
          break;
        case "r":
          (_a = this.#shown) == null ? void 0 : _a.webContents.reload();
          break;
        case "t":
          this.router.postMessage(["keybind:newtab", null]);
          break;
        case "w":
          this.router.postMessage(["keybind:closeactivetab", null]);
          break;
        default:
          return;
      }
    } else if (mods === ALT) {
      console.log(input.key);
      switch (input.key) {
        case "ArrowLeft":
          (_b = this.#shown) == null ? void 0 : _b.webContents.goBack();
          break;
        case "ArrowRight":
          (_c = this.#shown) == null ? void 0 : _c.webContents.goForward();
          break;
        default:
          return;
      }
    } else if (mods & CONTROL) {
      switch (input.key) {
        case "Tab":
          this.router.postMessage([
            mods & SHIFT ? "keybind:tableft" : "keybind:tabright",
            null
          ]);
          break;
        case "+":
          this.#iterShownWebContents((wc) => wc.zoomLevel < 9 && ++wc.zoomLevel);
          break;
        case "-":
          this.#iterShownWebContents((wc) => wc.zoomLevel > -9 && --wc.zoomLevel);
          break;
        case "0":
          this.#iterShownWebContents((wc) => wc.setZoomLevel(0));
          break;
        default:
          return;
      }
    } else {
      return;
    }
    ev.preventDefault();
  };
  show(id) {
    const view = this.#tabs.get(id);
    view && this.#shouldReallyShow(id, view);
  }
  delete(id) {
    this.#iterTabWebContents(id, (wc) => {
      wc.destroy();
      this.#tabs.delete(id);
    });
  }
  goto(id, url) {
    this.#iterTabWebContents(id, (wc) => wc.loadURL(url));
  }
  reload(id) {
    this.#iterTabWebContents(id, (wc) => wc.reload());
  }
  back(id) {
    this.#iterTabWebContents(id, (wc) => wc.goBack());
  }
  forward(id) {
    this.#iterTabWebContents(id, (wc) => wc.goForward());
  }
};

// src/electron/lib/flash.ts
init_define_import_meta_env();
var import_fs = __toModule(require("fs"));
var import_path = __toModule(require("path"));
var platformFileName = {
  win32: "pepflashplayer.dll",
  darwin: "PepperFlashPlayer.plugin",
  linux: "libpepflashplayer.so"
};
function findFlashPlugin() {
  const fileName = platformFileName[process.platform] ?? "flashplayer";
  const version = "32.0.0.465";
  const base = import_path.default.join(__dirname, "plugins", "flash");
  let path;
  path = import_path.default.join(base, `${process.platform}-${process.arch}`, fileName);
  if (import_fs.default.existsSync(path)) {
    return { path, version };
  }
  path = import_path.default.join(base, process.platform, fileName);
  if (import_fs.default.existsSync(path)) {
    return { path, version };
  }
  return null;
}
var mmsBase = {
  EOLUninstallDisable: 1,
  SilentAutoUpdateEnable: 0,
  EnableAllowList: 1,
  AutoUpdateDisable: 1,
  ErrorReportingEnable: 1,
  AllowListUrlPattern: [
    "blob:*",
    "file:*",
    "*://eternalfest.net",
    "*://*.eternalfest.net",
    "*://eternal-twin.net",
    "*://*.eternal-twin.net",
    "*://eternaltwin.net",
    "*://*.eternaltwin.net",
    "*://fevermap.org/",
    "*://*.fevermap.org/",
    "*://twinoid.com",
    "*://*.twinoid.com",
    "*://twinoid.es",
    "*://*.twinoid.es",
    "*://hammerfest.es",
    "*://*.hammerfest.es",
    "*://hammerfest.fr",
    "*://*.hammerfest.fr",
    "*://hfest.net",
    "*://*.hfest.net",
    "*://dinorpg.de",
    "*://*.dinorpg.de",
    "*://dinorpg.com",
    "*://*.dinorpg.com",
    "*://hordes.fr",
    "*://*.hordes.fr",
    "*://die2nite.com",
    "*://*.die2nite.com",
    "*://zombinoia.com",
    "*://*.zombinoia.com",
    "*://dieverdammten.de",
    "*://*.dieverdammten.de",
    "*://muxxu.com",
    "*://*.muxxu.com",
    "*://mush.vg",
    "*://*.mush.vg",
    "*://alphabounce.com",
    "*://*.alphabounce.com",
    "*://kadokado.com",
    "*://*.kadokado.com",
    "*://teacher-story.com",
    "*://*.teacher-story.com",
    "*://naturalchimie.com",
    "*://*.naturalchimie.com",
    "*://arkadeo.com",
    "*://*.arkadeo.com",
    "*://street-writer.com",
    "*://*.street-writer.com",
    "*://rockfaller.com",
    "*://*.rockfaller.com",
    "*://monster-hotel.net",
    "*://*.monster-hotel.net",
    "*://minitroopers.com",
    "*://*.minitroopers.com",
    "*://minitroopers.es",
    "*://*.minitroopers.es",
    "*://minitroopers.fr",
    "*://*.minitroopers.fr",
    "*://popotamo.com",
    "*://*.popotamo.com",
    "*://cafejeux.com",
    "*://*.cafejeux.com",
    "*://carapass.com",
    "*://*.carapass.com",
    "*://croquemonster.com",
    "*://*.croquemonster.com",
    "*://dinocard.net",
    "*://*.dinocard.net",
    "*://dinoparc.com",
    "*://*.dinoparc.com",
    "*://frutiparc.com",
    "*://*.frutiparc.com",
    "*://hyperliner.com",
    "*://*.hyperliner.com",
    "*://miniville.fr",
    "*://*.miniville.fr",
    "*://myminicity.com",
    "*://*.myminicity.com",
    "*://skywar.net",
    "*://*.skywar.net",
    "*://dailymotion.com",
    "*://*.dailymotion.com",
    "*://localhost",
    "*://*.localhost",
    "*://localhost:3000",
    "*://localhost:50317",
    "*://*.localhost:50317",
    "*://127.0.0.1",
    "*://*.127.0.0.1",
    "*://127.0.0.1:50317",
    "*://*.127.0.0.1:50317",
    "*://dinorpg.eternaltwin.org",
    "*://*.dinorpg.eternaltwin.org",
    "*://dinorpg.localhost",
    "*://*.dinorpg.localhost"
  ]
};
function formatMms(mms) {
  let blob = "";
  Object.keys(mms).sort().forEach((key) => {
    const value = mms[key];
    if (value instanceof Array) {
      value.forEach((v) => blob += `${key}=${v}
`);
    } else {
      blob += `${key}=${value}
`;
    }
  });
  return blob;
}
function generateMmsContents() {
  return formatMms(mmsBase);
}

// src/electron/lib/Settings.ts
init_define_import_meta_env();
var import_fs2 = __toModule(require("fs"));
var import_path2 = __toModule(require("path"));
var import_electron2 = __toModule(require("electron"));
var Settings = class {
  #userData = import_electron2.default.app.getPath("userData");
  #settingsPath = import_path2.default.join(this.#userData, "settings.json");
  bounds = {};
  constructor() {
    this.#touchFlashConfig();
    try {
      const buf = import_fs2.default.readFileSync(this.#settingsPath);
      const data = JSON.parse(buf.toString());
      this.bounds = data["bounds"];
    } catch {
    }
  }
  save() {
    const data = JSON.stringify({
      bounds: this.bounds
    });
    import_fs2.default.writeFileSync(this.#settingsPath, data);
  }
  #touchFlashConfig() {
    const systemDir = import_path2.default.join(this.#userData, "Pepper Data", "Shockwave Flash", "System");
    const mmsCfgPath = import_path2.default.join(systemDir, "mms.cfg");
    if (!import_fs2.default.existsSync(mmsCfgPath)) {
      import_fs2.default.mkdirSync(systemDir, { recursive: true });
      const mmsCfg = generateMmsContents();
      import_fs2.default.writeFileSync(mmsCfgPath, mmsCfg);
      console.log("wrote cfg to", mmsCfgPath);
    }
  }
};

// src/common/etwinApi.ts
init_define_import_meta_env();

// src/electron/lib/temp-games.json
var temp_games_default = [
  {
    title: "Eternal-Twin",
    link: "https://eternal-twin.net",
    icon: "https://eternal-twin.net/assets/etwin_icon.svg",
    items: [
      {
        title: "Eternalfest",
        link: "https://eternalfest.net"
      },
      {
        title: "MyHordes",
        link: "https://myhordes.eu",
        icon: "https://myhordes.eu/build/images/favicon.a07654e3.ico"
      },
      {
        title: "eMush",
        link: "https://beta.emush.eternal-twin.net",
        icon: "https://emush.eternaltwin.org/icon.ico"
      },
      {
        title: "Eternal DinoRPG",
        link: "https://dinorpg.eternaltwin.org"
      },
      {
        title: "Neoparc",
        link: "https://neoparc.eternal-twin.net"
      }
    ]
  },
  {
    title: "Twinoid",
    link: "https://twinoid.com",
    icon: "https://data.twinoid.com/img/design/logo_icon_large.png",
    items: [
      {
        title: "Alphabounce",
        link: "http://alphabounce.com"
      },
      {
        title: "Arkadeo",
        link: "http://arkadeo.com"
      },
      {
        title: "Croquemotel (DE)",
        link: "http://hotel.de.muxxu.com"
      },
      {
        title: "Croquemotel (EN)",
        link: "http://hotel.en.muxxu.com"
      },
      {
        title: "Croquemotel (FR)",
        link: "http://hotel.muxxu.com"
      },
      {
        title: "Die Verdammten",
        link: "http://www.dieverdammten.de"
      },
      {
        title: "Die2nite",
        link: "http://www.die2nite.com"
      },
      {
        title: "DinoRPG (DE)",
        link: "http://dinorpg.de",
        icon: "dead"
      },
      {
        title: "DinoRPG (EN)",
        link: "http://en.dinorpg.com"
      },
      {
        title: "DinoRPG (ES)",
        link: "http://es.dinorpg.com"
      },
      {
        title: "DinoRPG (FR)",
        link: "http://dinorpg.com"
      },
      {
        title: "El Bruto",
        link: "http://elbruto.muxxu.com"
      },
      {
        title: "Fever",
        link: "http://fever.muxxu.com"
      },
      {
        title: "FeverMap",
        link: "http://fevermap.org"
      },
      {
        title: "Hordes",
        link: "http://www.hordes.fr"
      },
      {
        title: "Intrusion",
        link: "http://intrusion.muxxu.com"
      },
      {
        title: "Kadokado",
        link: "http://kadokado.com"
      },
      {
        title: "Kingdom (DE)",
        link: "http://kingdom.de.muxxu.com"
      },
      {
        title: "Kingdom (EN)",
        link: "http://kingdom.en.muxxu.com"
      },
      {
        title: "Kingdom (FR)",
        link: "http://kingdom.muxxu.com"
      },
      {
        title: "Kube",
        link: "http://kube.muxxu.com"
      },
      {
        title: "La Brute",
        link: "http://labrute.muxxu.com"
      },
      {
        title: "Majority",
        link: "http://majority.muxxu.com"
      },
      {
        title: "Mein Brutalo",
        link: "http://meinbrutalo.muxxu.com"
      },
      {
        title: "Monster Hotel",
        link: "http://monster-hotel.net"
      },
      {
        title: "Motion-Ball 2",
        link: "http://mb2.muxxu.com"
      },
      {
        title: "Mush (EN)",
        link: "http://mush.twinoid.com"
      },
      {
        title: "Mush (ES)",
        link: "http://mush.twinoid.es",
        icon: "dead"
      },
      {
        title: "Mush (FR)",
        link: "http://mush.vg"
      },
      {
        title: "MyBrute",
        link: "http://mybrute.muxxu.com"
      },
      {
        title: "Naturalchimie",
        link: "http://naturalchimie.com"
      },
      {
        title: "Odyssee",
        link: "http://odyssey.muxxu.com"
      },
      {
        title: "Rockfaller Journey",
        link: "http://rockfaller.com"
      },
      {
        title: "Snake (FR)",
        link: "http://snake.muxxu.com"
      },
      {
        title: "Snake (EN)",
        link: "http://snake.en.muxxu.com"
      },
      {
        title: "Snake (ES)",
        link: "http://snake.es.muxxu.com"
      },
      {
        title: "Snake (DE)",
        link: "http://snake.de.muxxu.com"
      },
      {
        title: "Street Writer",
        link: "http://street-writer.com"
      },
      {
        title: "Studio Quiz",
        link: "http://quiz.muxxu.com"
      },
      {
        title: "Teacher Story",
        link: "http://teacher-story.com"
      },
      {
        title: "Zombinoia",
        link: "http://www.zombinoia.com"
      }
    ]
  },
  {
    title: "Hammerfest",
    link: "http://www.hammerfest.fr",
    items: [
      {
        title: "Hammerfest (FR)",
        link: "http://www.hammerfest.fr"
      },
      {
        title: "Hammerfest (EN)",
        link: "http://www.hfest.net",
        icon: "dead"
      },
      {
        title: "Hammerfest (ES)",
        link: "http://www.hammerfest.es",
        icon: "dead"
      }
    ]
  },
  {
    title: "Dinoparc",
    link: "http://www.dinoparc.com",
    items: [
      {
        title: "Dinoparc (FR)",
        link: "http://www.dinoparc.com"
      },
      {
        title: "Dinoparc (EN)",
        link: "http://en.dinoparc.com"
      },
      {
        title: "Dinoparc (ES)",
        link: "http://sp.dinoparc.com"
      }
    ]
  },
  {
    title: "Motion-Twin",
    link: "https://motion-twin.com",
    icon: "",
    items: [
      {
        title: "Carapass",
        link: "http://carapass.com"
      },
      {
        title: "Croquemonster",
        link: "http://croquemonster.com"
      },
      {
        title: "Dinocard",
        link: "http://dinocard.net"
      },
      {
        title: "Hyperliner",
        link: "http://hyperliner.com"
      },
      {
        title: "Minitroopers (FR)",
        link: "http://minitroopers.fr"
      },
      {
        title: "Minitroopers (EN)",
        link: "http://minitroopers.com"
      },
      {
        title: "Minitroopers (ES)",
        link: "http://minitroopers.es"
      },
      {
        title: "Miniville (FR)",
        link: "http://miniville.fr"
      },
      {
        title: "MyMiniCity (EN)",
        link: "http://myminicity.com"
      },
      {
        title: "MyMiniCity (ES)",
        link: "http://myminicity.es",
        icon: "dead"
      },
      {
        title: "Popotamo",
        link: "http://www.popotamo.com"
      },
      {
        title: "Skywar",
        link: "http://skywar.net"
      }
    ]
  }
];

// src/common/etwinApi.ts
var HOST = "https://eternal-twin.net";
var ROOT = HOST + "/api/v1";
async function getAppReleases(fetch2) {
  const url = ROOT + "/app/releases";
  const res = await fetch2(url, {
    requestOptions: { rejectUnauthorized: false }
  });
  return res.json();
}
async function getGames(_) {
  return temp_games_default;
}

// src/common/fetch.ts
init_define_import_meta_env();
var Https = import("https").catch(() => ({}));
var Response = class {
  constructor(status, headers, bodyStream) {
    this.status = status;
    this.headers = headers;
    this.bodyStream = bodyStream;
  }
  text() {
    const [type, stream] = this.bodyStream;
    switch (type) {
      case 0:
        return Promise.resolve(stream);
      case 1:
        return new Promise((resolve) => {
          let data = "";
          stream.on("data", (chunk) => data += chunk.toString());
          stream.on("end", () => resolve(data));
        });
      case 2:
        return stream.text();
    }
  }
  json() {
    return this.text().then(JSON.parse);
  }
};
var nodeFetch = (url, o = {}) => Https.then((Https2) => new Promise((resolve) => {
  const req = Https2.request(url, __spreadValues({
    headers: o.headers,
    method: o.method
  }, o.requestOptions));
  req.on("response", (res) => resolve(new Response(res.statusCode, res.headers, [1, res])));
  req.on("error", (err) => resolve(new Response(0, {}, [0, err.message])));
  o.body && req.write(o.body);
  req.end();
}));

// src/electron/index.ts
var settings = new Settings();
var flash = findFlashPlugin();
if (flash) {
  import_electron3.default.app.commandLine.appendSwitch("no-sandbox");
  import_electron3.default.app.commandLine.appendSwitch("ppapi-flash-path", flash.path);
  import_electron3.default.app.commandLine.appendSwitch("ppapi-flash-version", flash.version);
}
import_electron3.default.Menu.setApplicationMenu(null);
function wrapRouter(router) {
  const replaceUrl = (o, rx, s) => o.url = o.url.replace(rx, s);
  const { on, postMessage } = router;
  router.on = (event, listener) => event === "message" ? on.call(router, event, (ev) => {
    const pl = ev.data[1];
    "url" in pl && replaceUrl(pl, /^etwin:/, "about:blank#");
    return listener(ev);
  }) : on.call(router, event, listener);
  router.postMessage = (message) => {
    const pl = message[1];
    pl && "url" in pl && replaceUrl(pl, /^about:blank#/, "etwin:");
    postMessage.call(router, message);
  };
  return router;
}
async function queryGames(router) {
  const games = await getGames(nodeFetch);
  router.postMessage(["data:games", { games }]);
  setTimeout(() => queryGames(router), 1e3 * 60 * 30);
}
async function queryLastVersion(router) {
  const { latest } = await getAppReleases(nodeFetch);
  console.log({ lhs: latest.version, rhs: define_import_meta_env_default.PACKAGE_VERSION });
  if ((0, import_gt.default)(latest.version, define_import_meta_env_default.PACKAGE_VERSION)) {
    router.postMessage(["data:update", { newVersion: latest.version }]);
  }
  setTimeout(() => queryLastVersion(router), 1e3 * 60 * 30);
}
function createWindow() {
  const win = new import_electron3.default.BrowserWindow({
    width: settings.bounds.width,
    height: settings.bounds.height,
    x: settings.bounds.x,
    y: settings.bounds.y,
    webPreferences: {
      defaultEncoding: "utf8",
      contextIsolation: true,
      nodeIntegration: false,
      preload: import_path3.default.join(import_electron3.default.app.getAppPath(), "preload.js"),
      sandbox: true,
      plugins: true
    }
  });
  if (define_import_meta_env_default.DEV) {
    win.loadURL(`http://localhost:${process.env["PORT"] ?? 3e3}`);
    win.webContents.openDevTools({ mode: "detach" });
  }
  win.on("close", () => {
    settings.bounds = win.getBounds();
  });
  import_electron3.default.ipcMain.once("router", ({ ports }) => {
    const router = wrapRouter(ports[0]);
    router.start();
    queryGames(router);
    queryLastVersion(router);
    const tabManager = new BrowserViewManager(win, router);
    router.on("message", (ev) => {
      const [type, pl] = ev.data;
      define_import_meta_env_default.DEV && console.dir({ type, pl });
      switch (type) {
        case "tab:new":
          tabManager.new(pl.id);
          tabManager.goto(pl.id, pl.url);
          break;
        case "tab:show":
          tabManager.show(pl.id);
          break;
        case "tab:goto":
          tabManager.goto(pl.id, pl.url);
          break;
        case "tab:close":
          tabManager.delete(pl.id);
          break;
        case "tab:reload":
          tabManager.reload(pl.id);
          break;
        case "tab:back":
          tabManager.back(pl.id);
          break;
        case "tab:forward":
          tabManager.forward(pl.id);
          break;
      }
    });
    win.webContents.on("before-input-event", tabManager.inputListener);
  });
}
import_electron3.default.app.on("will-quit", () => {
  settings.save();
});
import_electron3.default.app.on("ready", () => {
  createWindow();
});
import_electron3.default.app.on("web-contents-created", (_, webContents) => {
  webContents.on("select-bluetooth-device", (event, _2, callback) => {
    event.preventDefault();
    callback("");
  });
});
