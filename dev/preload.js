var __create = Object.create;
var __defProp = Object.defineProperty;
var __getOwnPropDesc = Object.getOwnPropertyDescriptor;
var __getOwnPropNames = Object.getOwnPropertyNames;
var __getProtoOf = Object.getPrototypeOf;
var __hasOwnProp = Object.prototype.hasOwnProperty;
var __markAsModule = (target) => __defProp(target, "__esModule", { value: true });
var __reExport = (target, module2, desc) => {
  if (module2 && typeof module2 === "object" || typeof module2 === "function") {
    for (let key of __getOwnPropNames(module2))
      if (!__hasOwnProp.call(target, key) && key !== "default")
        __defProp(target, key, { get: () => module2[key], enumerable: !(desc = __getOwnPropDesc(module2, key)) || desc.enumerable });
  }
  return target;
};
var __toModule = (module2) => {
  return __reExport(__markAsModule(__defProp(module2 != null ? __create(__getProtoOf(module2)) : {}, "default", module2 && module2.__esModule && "default" in module2 ? { get: () => module2.default, enumerable: true } : { value: module2, enumerable: true })), module2);
};

// <define:import.meta.env>
var DEV = true;
var PACKAGE_VERSION = "0.0.0+uwuwuwu";
var define_import_meta_env_default = { DEV, PACKAGE_VERSION };

// src/electron/preload.ts
var import_electron = __toModule(require("electron"));
function getFlashVersion() {
  var _a, _b;
  if (navigator.plugins) {
    const plugin = (_b = (_a = navigator.plugins).namedItem) == null ? void 0 : _b.call(_a, "Shockwave Flash");
    if (plugin) {
      return plugin.description;
    }
  }
  return null;
}
var { port1: dealer, port2: router } = new MessageChannel();
import_electron.ipcRenderer.postMessage("router", null, [router]);
import_electron.contextBridge.exposeInMainWorld("etwin", {
  versions: {
    etwin: define_import_meta_env_default.PACKAGE_VERSION,
    chrome: process.versions.chrome,
    electron: process.versions.electron,
    node: process.versions.node,
    flash: getFlashVersion()
  },
  onmessage(handler) {
    dealer.onmessage = ({ data }) => handler(data);
  },
  send(message) {
    dealer.postMessage(message);
  },
  env: "electron"
});
